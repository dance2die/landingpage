module.exports = {
  siteMetadata: {
    title: `Welcome to Sung's World 🚀`,
  },
  plugins: ['gatsby-plugin-react-helmet'],
}
