# gatsby-starter-default

The default Gatsby starter.

For an overview of the project structure please refer to the [Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/).

## Icons

* [Blog (SVG)](https://thenounproject.com/search/?q=blog&i=1448098) - Blog by Mello from the Noun Project
* [Creations (SVG)](https://thenounproject.com/search/?q=creations&i=516844) - creativity by Becris from the Noun Project
* [Contributions (SVG)](https://thenounproject.com/search/?q=contributions&i=1736176) - teamwork by vectoriconset10 from the Noun Project

## Install

Make sure that you have the Gatsby CLI program installed:

```sh
npm install --global gatsby-cli
```

And run from your CLI:

```sh
gatsby new gatsby-example-site
```

Then you can run it by:

```sh
cd gatsby-example-site
gatsby develop
```

## Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/gatsbyjs/gatsby-starter-default)
